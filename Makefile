CERTDIR = certs
CADIR = ca

help:
	@echo "init - Create all the certs, must be run ONLY ONCE"
	@echo "update - Update the repository and the submodules"

init: ca wauths waccountc wgws javaclient
	git submodule init
	git submodule update

update:
	git pull
	git submodule update --remote

certdir:
	mkdir -p $(CERTDIR)/{$(CADIR),server,client,}

.ONESHELL:
ca: certdir
	cd $(CERTDIR)/$(CADIR)
	openssl genrsa -out ca.key 2048
	chmod 400 ca.key
	openssl req -new -x509 -sha256 -key ca.key -out ca.crt -days 3650 -subj '/CN=Weepen Weepis'
	chmod 444 ca.crt

.ONESHELL:
wauths: certdir
	cd $(CERTDIR)
	openssl genrsa -out server/weepi_auth.key 2048
	chmod 400 server/weepi_auth.key
	openssl req -new -key server/weepi_auth.key -sha256 -out server/weepi_auth.csr -subj '/CN=auth'
	openssl x509 -req -days 365 -sha256 -in server/weepi_auth.csr -CA ca/ca.crt -CAkey ca/ca.key -set_serial 1 -out server/weepi_auth.crt

.ONESHELL:
waccountc: certdir
	cd $(CERTDIR)
	openssl genrsa -out client/weepi_auth.key 2048
	openssl req -new -key client/weepi_auth.key -out client/weepi_auth.csr -subj '/CN=account'
	openssl x509 -req -days 365 -sha256 -in client/weepi_auth.csr -CA ca/ca.crt -CAkey ca/ca.key -set_serial 2 -out client/weepi_auth.crt

.ONESHELL:
wgws: certdir
	cd $(CERTDIR)
	openssl genrsa -out server/weepi_gw.key 2048
	openssl req -new -key server/weepi_gw.key -sha256 -out server/weepi_gw.csr -subj '/CN=gateway'
	openssl x509 -req -days 365 -sha256 -in server/weepi_gw.csr -CA ca/ca.crt -CAkey ca/ca.key -set_serial 3 -out server/weepi_gw.crt

.ONESHELL:
javaclient: certdir
	cd $(CERTDIR)
	openssl pkcs8 -topk8 -inform PEM -outform PEM -in client/weepi_auth.key -out client/weepi_auth.java.key -nocrypt
