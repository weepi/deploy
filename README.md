# Weepi deploy !

The weepi deploy repository was used by [Weepen](https://weepen.io/) to manage their microservices (weepis).

[Docker Compose](https://docs.docker.com/compose/) was used to run them.
